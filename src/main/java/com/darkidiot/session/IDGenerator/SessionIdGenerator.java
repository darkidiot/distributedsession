package com.darkidiot.session.IDGenerator;

import javax.servlet.http.HttpServletRequest;

/**
 * 分布式ID生成策略接口
 * Copyright (c) for darkidiot
 * Date:2017/4/14
 * Author: <a href="darkidiot@icloud.com">darkidiot</a>
 * School: CUIT
 * Desc:
 */
public interface SessionIdGenerator {
    /**
     * 获取唯一ID
     * @param paramHttpServletRequest
     * @return
     */
    String generateId(HttpServletRequest paramHttpServletRequest);
}
