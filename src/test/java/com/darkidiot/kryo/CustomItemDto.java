package com.darkidiot.kryo;

import lombok.Data;

import java.io.Serializable;

/**
 * Copyright (c) for darkidiot
 * Date:2017/4/16
 * Author: <a href="darkidiot@icloud.com">darkidiot</a>
 * School: CUIT
 * Desc:
 */
@Data
class CustomItemDto implements Serializable{

    private long id;
    private String itemCode;
    private String itemMemo;
    private String itemName;
    private double itemPrice;
    private int sort;
}
