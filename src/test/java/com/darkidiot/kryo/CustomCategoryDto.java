package com.darkidiot.kryo;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Copyright (c) for darkidiot
 * Date:2017/4/19
 * Author: <a href="darkidiot@icloud.com">darkidiot</a>
 * School: CUIT
 * Desc:
 */
@Data
class CustomCategoryDto implements Serializable{
    private String categoryCode;
    private String categoryName;
    private List<CustomItemDto> customItemList = Lists.newArrayList();
    private Set<CustomItemDto> customItemSet = Sets.newHashSet();
    private Map<String, CustomItemDto> customItemMap = Maps.newHashMap();
}
