package com.darkidiot.kryo;

import com.darkidiot.session.serialize.JsonSerializer;
import com.darkidiot.session.serialize.KryoSerializer;
import com.darkidiot.session.serialize.Serializer;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.Map;

/**
 * IntegrationTest 测试类
 * Copyright (c) 2016 成都国美大数据
 * Date:2017/4/26
 * Author: 2016年 <a href="heqiao2@gome.com.cn">darkidiot</a>
 * Desc:
 */
@Slf4j
public class IntegrationTest {

    private static Jedis jedis;

    private static String KEY_FOR_TEST = "distributed-session:6788ae38-16d2a2e6-15ba9cc6010-nyamsEpg";

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        jedis = new Jedis("10.128.31.85", 2222);
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        jedis.disconnect();
    }


    @Test
    public void testSerializer() {
        Map<String, Object> map = Maps.newHashMap();
        map.put("javamelody.userAgent", "Mozilla / 5.0 (Linux; Android 6.0; Nexus 5 Build / MRA58N)AppleWebKit / 537.36 (KHTML, like Gecko)Chrome / 46.0 .2490 .76 Mobile Safari/537.36");
        map.put("javamelody.remoteAddr", "127.0 .0 .1 forwarded for 10.122 .1 .64");
        map.put("session_user_id", 39034L);
        map.put("javamelody.country", "CN");
        Serializer kryoSerializer = new KryoSerializer();
        String serialize = kryoSerializer.serialize(map);
        log.info(serialize);
        jedis.setex(KEY_FOR_TEST, 604880, serialize);
        Map<String, Object> deserialize = kryoSerializer.deserialize(serialize);
        log.info(deserialize + "");
        serialize = jedis.get(KEY_FOR_TEST);
        deserialize = kryoSerializer.deserialize(serialize);
        log.info(deserialize + "");
    }

    @Test
    public void testGson() {
        Map<String, Object> map = Maps.newHashMap();
        map.put("javamelody.userAgent", "Mozilla / 5.0 (Linux; Android 6.0; Nexus 5 Build / MRA58N)AppleWebKit / 537.36 (KHTML, like Gecko)Chrome / 46.0 .2490 .76 Mobile Safari/537.36");
        map.put("javamelody.remoteAddr", "127.0 .0 .1 forwarded for 10.122 .1 .64");
        map.put("session_user_id", 39034L);
        map.put("javamelody.country", "CN");
        Serializer kryoSerializer = new JsonSerializer();
        String serialize = kryoSerializer.serialize(map);
        log.info(serialize);
        jedis.setex(KEY_FOR_TEST, 604880, serialize);
        Map<String, Object> deserialize = kryoSerializer.deserialize(serialize);
        log.info(deserialize + "");
        serialize = jedis.get(KEY_FOR_TEST);
        deserialize = kryoSerializer.deserialize(serialize);
        log.info(deserialize + "");
    }
}
